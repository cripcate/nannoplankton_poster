# Introduction

The Triassic-Jurassic mass extinction was most likely caused by global warming and ocean acidification linked to CAMP volcanism. These conditions led to a calcification crisis and the associated selective demise of supercalcifying organisms like scleractinian corals and calcareous nannofossils (CN). Despite the logical coherence of this thesis, detailed nannoplankton data from the boundary interval is lacking.
In this study we examine three marine boundary successions in the Northern Calcareous Alps (NCA) in regard to preservation and diversity of cn. We aim to answer the key questions:
1. Is primary producer collapse a plausible trigger for the Triassic-Jurassic mass-extinction?
2. Do preservational status and size of organisms allow conclusions about acidification of the paleo-ocean?
3. What roes does CN play in carbon cycling before, during, and in the aftermath of the extinction event?

# Stratigraphy

The idealized stratigraphic column of the working area is shown in Fig. 4. A salinity-controlled intraplatform-basin was the dominant facies in the Rhaetian Kössen-beds. A facies change occurs at the initial extinction (high C_org) and limestone sedimentation comes to a halt.

Towards the Jurassic, carbonate content rises again, forming the reddish *caliphyllum* beds. Carbonate cycling further recovers in the Scheibelberg-formation. Samples were taken throughout the section and analysed in respect of carbon content and nannofossil distribution.

# Calcareous Nannofossils

The Rhaetian nannofacies of the Kössen-beds is dominated by calcispheres. Specifically P. triassica is abundant and very massive. Conuspheres are more scarce but present throughout the Rhaetian. Coccoliths are only seldom represented and often poorly preserved. They are usually identified by their central structures, which suffered from severe dissolution and recrystallization in the examined samples. Conuspheres and Calcispheres however, are in a better preservational state and clearly identifiable. 

At the top of the Kössen beds there is a sudden drop in CN abundance. This occurs simultaneously to an abrupt fall in carbonate content, noticeable in the formation of brownish micrite aggregates in smear-slides. The few persisting fossils show a terrible preservation and are often not distinguishable. Nannofossils seemingly die off slightly before the "calcification crisis".

Samples of the T-Bed show high organic content and almost no calcite bodies. 

# Discussion 

# Figure descriptions

Fig. 1 - Conuspheres: Truncated conical nannofossils of 2 - 8 µm length, with a central channel, an inner and outer calcite layer

Fig. 2 - Obliquipithonella rhombica, Calcisphere, 8 - 10 µm diameter, hollow calcite sphere with chaotically arranged outer layer of rhombic crystals

Fig. 3 - Orthopithonella rhombica, Calcisphere, 8 - 10 µm diameter, hollow calcite body with an outer shell of orthogonal crystals

Fig. 4 - Prinsiosphaera triassica, Calcisphere, 5 - 14 µm diameter, massive calcite body composed of clusters of parallel lamellae;

Calcispheres: Resting cysts of dinoflagellates with a ~8 - 12 µm diameter

Fig. 3 - Coccoliths: Ca. 1 - 8 µm long, elliptical calcite plates from Coccolithophore tests

Fig. 4 - Idealized stratigraphy, compiled from the three examined sections. Arrows on ammonite symbols denote first- or last-occurrence: p.s. = Psiloceras spelae; c.m. = Choristoceras marshi; c.p. = Calliphyloceras pacificum;

Fig. 5 - Field impressions: A = Koessen-beds, Rhonberg; B = T-bed, Boundary-beds & Schattwald-beds, Rhonberg; C = Kössen-beds, Boundary-beds, Nudelgraben; D = Boundary-beds, Nudelgraben; E = Calliphyllum-bed, Rhonberg; F = Scheibelberg-formation, Nudelgraben;

Fig. 6 - Macrofossils: A = Oxycolpella oxyceupos, Middle Triassic; B = Calliphylloceras pacificum; C = Assemblage of C. marshi &  Meleagrinella sp.; D = Eopsiloceras spelae, lowest Jurassic ammonite; E = Agerchlamys sp.; F = Choristoceras marshi;


Fig. 6 - Microscope images of typical nannofossil assemblages. A) Transmission light microscopy, B) Transmission light with crossed polarizers, C) Scanning electron microscopy;
1: Rhaetian Kössen-beds, sample from the Lahnewiesgraben-section; 
A) Red = Conuspheres, Blue = Coccoliths, Green = Calcispheres; 
B) az = Archaeozygodiscus koessensis, cm = Crucirhabdus minutus, cp = Crucirhabdus primulus, ez = Eoconusphaera zlambachensis, og = Orthopithonella geometrica, or = Orthopithonella rhombica, pt = Prinsiosphaera triassica;
C) Arrows pointing to biogenic crystal-structures;
2: T-Bed, initial extinction phase, sample from the Rhonberg section; 
C) Arrows pointing to biogenic crystal-structures.
3: Schattwald-beds, main extinction phase 
A) Sample from the Nudelgraben section 
C) Sample from the Rhonberg section; arrows pointing to biogenic crystal-structures.

# Acknowledgements

First I'd like to thank my supervisors J. Reitner and J. Duda for the idea and intense support on this study, as well as the organizational and motivational support without which I wouldn't be here. I'd like to thank Dorothea Hause-Reitner, Birgit Röhring, Wolfgang Dröse and Axel Hackmann for analytical support, and Efi and Kurt Kment for assistance in field work. Special thanks to The Paleontological Society of China and The Universitätsbund Göttingen e.V. for financial support.

# References