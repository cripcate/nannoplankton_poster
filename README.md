# Poster Presentation of my Master Thesis

Used for the 2nd Joint Conference of the Paleontological Society of China (PSC) and the Paläontologische Gesellschaft (PalGes). The `web.png` file contains the poster, the `text.md` file contains all the text on the poster. For an `.svg`-version, or any other file format of the poster, please contact me at nils.hoeche@uni-goettingen.de.

